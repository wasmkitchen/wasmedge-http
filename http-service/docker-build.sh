#!/bin/bash

# Activate the feature https://docs.docker.com/desktop/containerd/
docker buildx build --platform wasi/wasm32 -t wasmservice .

